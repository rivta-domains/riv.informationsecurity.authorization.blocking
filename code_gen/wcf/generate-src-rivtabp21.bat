
	@REM ---------------------------------------------------------------------------------
	@REM Generates c# WCF service contracts (interface), client proxies and wcf config
	@REM file for the WSDLs + XML Schemas of the service domain, using .Net WCF tool svcuti.exe
	@REM ---------------------------------------------------------------------------------
	@REM Licensed to the Apache Software Foundation (ASF) under one
	@REM or more contributor license agreements. See the NOTICE file
	@REM distributed with this work for additional information
	@REM regarding copyright ownership. Inera AB licenses this file
	@REM to you under the Apache License, Version 2.0 (the
	@REM "License"); you may not use this file except in compliance
	@REM with the License. You may obtain a copy of the License at
	@REM
	@REM http://www.apache.org/licenses/LICENSE-2.0
	@REM Unless required by applicable law or agreed to in writing,
	@REM software distributed under the License is distributed on an
	@REM "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
	@REM KIND, either express or implied. See the License for the
	@REM specific language governing permissions and limitations
	@REM under the License.
	@REM ---------------------------------------------------------------------------------
	CD ..\..
	
	SET SCHEMADIR=schemas
	
	SET W0=%SCHEMADIR%\interactions\CancelTemporaryExtendedRevokeInteraction\CancelTemporaryExtendedRevokeInteraction_4.0_RIVTABP21.wsdl
SET X0=%SCHEMADIR%\interactions\CancelTemporaryExtendedRevokeInteraction\*.xsd

SET W1=%SCHEMADIR%\interactions\CheckBlocksInteraction\CheckBlocksInteraction_4.0_RIVTABP21.wsdl
SET X1=%SCHEMADIR%\interactions\CheckBlocksInteraction\*.xsd

SET W2=%SCHEMADIR%\interactions\DeleteExtendedBlockInteraction\DeleteExtendedBlockInteraction_4.0_RIVTABP21.wsdl
SET X2=%SCHEMADIR%\interactions\DeleteExtendedBlockInteraction\*.xsd

SET W3=%SCHEMADIR%\interactions\GetBlocksInteraction\GetBlocksInteraction_4.0_RIVTABP21.wsdl
SET X3=%SCHEMADIR%\interactions\GetBlocksInteraction\*.xsd

SET W4=%SCHEMADIR%\interactions\GetExtendedBlocksForPatientInteraction\GetExtendedBlocksForPatientInteraction_4.0_RIVTABP21.wsdl
SET X4=%SCHEMADIR%\interactions\GetExtendedBlocksForPatientInteraction\*.xsd

SET W5=%SCHEMADIR%\interactions\GetPatientIdsInteraction\GetPatientIdsInteraction_4.0_RIVTABP21.wsdl
SET X5=%SCHEMADIR%\interactions\GetPatientIdsInteraction\*.xsd

SET W6=%SCHEMADIR%\interactions\RegisterBlockInteraction\RegisterBlockInteraction_4.0_RIVTABP21.wsdl
SET X6=%SCHEMADIR%\interactions\RegisterBlockInteraction\*.xsd

SET W7=%SCHEMADIR%\interactions\RegisterExtendedBlockInteraction\RegisterExtendedBlockInteraction_4.0_RIVTABP21.wsdl
SET X7=%SCHEMADIR%\interactions\RegisterExtendedBlockInteraction\*.xsd

SET W8=%SCHEMADIR%\interactions\RegisterTemporaryExtendedRevokeInteraction\RegisterTemporaryExtendedRevokeInteraction_4.0_RIVTABP21.wsdl
SET X8=%SCHEMADIR%\interactions\RegisterTemporaryExtendedRevokeInteraction\*.xsd

SET W9=%SCHEMADIR%\interactions\RegisterTemporaryRevokeInteraction\RegisterTemporaryRevokeInteraction_4.0_RIVTABP21.wsdl
SET X9=%SCHEMADIR%\interactions\RegisterTemporaryRevokeInteraction\*.xsd

SET W10=%SCHEMADIR%\interactions\RevokeExtendedBlockInteraction\RevokeExtendedBlockInteraction_4.0_RIVTABP21.wsdl
SET X10=%SCHEMADIR%\interactions\RevokeExtendedBlockInteraction\*.xsd

SET W11=%SCHEMADIR%\interactions\UnregisterBlockInteraction\UnregisterBlockInteraction_4.0_RIVTABP21.wsdl
SET X11=%SCHEMADIR%\interactions\UnregisterBlockInteraction\*.xsd

SET W12=%SCHEMADIR%\interactions\UnregisterTemporaryRevokeInteraction\UnregisterTemporaryRevokeInteraction_4.0_RIVTABP21.wsdl
SET X12=%SCHEMADIR%\interactions\UnregisterTemporaryRevokeInteraction\*.xsd

SET XCORE=%SCHEMADIR%\core_components\*.xsd

SET SCHEMAS=%XCORE% %W0% %X0% %W1% %X1% %W2% %X2% %W3% %X3% %W4% %X4% %W5% %X5% %W6% %X6% %W7% %X7% %W8% %X8% %W9% %X9% %W10% %X10% %W11% %X11% %W12% %X12% 

SET OUTFILE=code_gen\wcf\generated-src\InformationsecurityAuthorizationBlockingInteractions.cs
SET APPCONFIG=/config:code_gen\wcf\generated-src\app.config
SET NAMESPACE=/namespace:*,Riv.Informationsecurity.Authorization.Blocking.Schemas.v4
SET SVCUTIL="svcutil.exe"
%SVCUTIL% /language:cs /syncOnly /out:%OUTFILE% %APPCONFIG% %NAMESPACE% %SCHEMAS%

ECHO Adding #pragma warning disable 1591 to %OUTFILE%
ECHO #pragma warning disable 1591 > %OUTFILE%.tmp
TYPE %OUTFILE% >> %OUTFILE%.tmp
MOVE /Y %OUTFILE%.tmp %OUTFILE%

CD code_gen\wcf
ECHO Generating Service contract .Net Binding interfaces and classes for informationsecurity:authorization:blocking Release 4
ECHO I DotNetprojektet ska du ta lagga till referens till System.ServiceModel
